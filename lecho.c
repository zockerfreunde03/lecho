#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

typedef unsigned long long ull;

char *read_file(const char *);
void print_line(char *, ull);
off_t file_size(const char *);
ull count_lines(char *);

struct options *parse_args(int, char **);
int *last_args(int, char **);

#define EQUALS(x, y, z) !strcmp(x, y) || !strcmp(x, z)

struct options {
	char *file;
	ull line;
	char _help;
};

/*
 * Parse argv for LINE and FILE and maybe help
 * 
 * if -h/--help is given we exit early
 *
 * On all errors we return NULL
 */
struct options *parse_args(int argc, char **argv)
{
	struct options *o = calloc(1, sizeof(struct options));

	if (!o) {
		perror("lecho - parse_args");
		goto abort;
	}

	int file_flag = 0;
	int line_flag = 0;


	for (int i = 1; i < argc; ++i) {
		if (EQUALS(argv[i], "-h", "--help")) {
			o->_help = 1;
			break;
		} else if (EQUALS(argv[i], "-f", "--file")) {
			file_flag = i;
		} else if (EQUALS(argv[i], "-l", "--line")) {
			line_flag = i;
		}
	}

	if (o->_help)
		return o;

	if (file_flag) {
		if (argc > file_flag + 1) {
			o->file = argv[file_flag + 1];
		} else {
			fprintf(stderr, "--file requires an argument\n");
			goto abort;
		}
	}

	if (line_flag) {
		if (argc > line_flag + 1) {
			o->line = atoll(argv[line_flag + 1]);
		} else {
			fprintf(stderr, "--line requires an argument\n");
			goto abort;
		}
	}

	if (line_flag && file_flag)
		goto check_line;

	int *last = last_args(argc, argv);

	if (!last)
		goto abort;

	if (!line_flag && !file_flag) {
		o->file = argv[last[0]];
		o->line = atoll(argv[last[1]]);
	} else if (!line_flag) {
		o->line = atoll(argv[last[1]]);
	} else {
		o->file = argv[last[1]];
	}

	free(last);

check_line:
	if (o->line == 0) {
		fprintf(stderr,
			"Please provide a non-zero positive integer as a line number\n");
		goto abort;
	}

	return o;

abort:
	free(o);
	return NULL;
}

/*
 * get the index of the last two freestanding arguments. 
 *
 * Freestanding means they do not follow after an explicit option for file/line
 */
int *last_args(int argc, char **argv)
{
	int one = 0;
	int two = 0;
	for (int i = 1; i < argc; ++i) {
		if (EQUALS(argv[i], "-f", "--file") ||
		    EQUALS(argv[i], "-l", "--line")) {
			++i;
			continue;
		}

		one = two;
		two = i;
	}

	if (!two)
		return NULL;

	int *ret = calloc(2, sizeof(int));

	if (!ret) {
		perror("lecho - last_args");
		free(ret);
		return NULL;
	}

	ret[0] = one;
	ret[1] = two;

	return ret;
}

/*
 * Count lines present in the file by counting all '\n' that are found.
 */
ull count_lines(char *s)
{
	ull eol = 0;

	for (int i = 0; s[i] != '\0'; ++i)
		eol += s[i] == '\n';

	return eol;
}

/*
 * Print a specific line from a file. The line is found by counting all newlines
 * until we are  one before the desired line, then we print that line
 * character-by-character until the next newline. If there are not enough lines
 * in the file, inform the user and return.
 */
void print_line(char *s, ull line)
{
	ull max = count_lines(s);

	if (max < line) {
		fprintf(stderr,
			"Cannot print line %lld from a file with %lld line(s)\n",
			line, max);
		return;
	}

	ull current_line = 0;
	for (int i = 0; s[i] != '\0'; ++i) {
		if (current_line == line - 1) {
			printf("%c", s[i]);
			if (s[i] == '\n')
				break;
		} else {
			current_line += s[i] == '\n';
		}
	}
}

/*
 * Uses sys/stat.h to get the size of the file at path
 * Returns -1 on error
 */
off_t file_size(const char *path)
{
	struct stat st;

	if (stat(path, &st) == 0)
		return st.st_size;

	return -1;
}

/*
 * Read the contents of `path`
 * Returns either the contents of `path` or NULL on any error
 */
char *read_file(const char *path)
{
	off_t _file_size = file_size(path);

	if (_file_size == -1) {
		perror("lecho - read_file");

		return NULL;
	}

	FILE *fp = fopen(path, "r");

	if (!fp) {
		perror("lecho - read_file");

		return NULL;
	}

	char *input = calloc(_file_size + 1, sizeof(char));

	if (!input)
		goto error;

	int num_read = fread(input, sizeof(char), _file_size, fp);

	if (num_read < _file_size)
		goto error;

	if (ferror(fp))
		goto error;

	fclose(fp);

	return input;

error:
	perror("lecho - read_file");

	fclose(fp);
	free(input);

	return NULL;
}

int main(int argc, char **argv)
{
	struct options *o = parse_args(argc, argv);

	if (!o)
		return 1;

	if (o->_help) {
		fprintf(stderr, "%s FILE LINE\n", argv[0]);
		free(o);
		return 0;
	}

	char *file = read_file(o->file);

	if (!file) {
		free(o);
		return 1;
	}

	print_line(file, o->line);

	free(file);
	free(o);
	return 0;
}
